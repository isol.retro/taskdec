<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomsDeclaration;
use App\CustomsDeclarationGoods;
use Mail;

class MailBlastController extends Controller
{
    public function sentMailBlast()
    {
        $objCustomsDeclaration = CustomsDeclaration::with(['nationality','customsGoods'])->find('d376ccbb-9c85-409a-a305-160e6bd7945e');
        Mail::send('component.mail.confirm',
            [
                'objCustomsDeclaration' => $objCustomsDeclaration
            ], 
            function ($message) use ($objCustomsDeclaration){
                $message->from('rnd.ediindonesia@gmail.com', 'Customs Officer');
                $message->to($objCustomsDeclaration->cd_email, $objCustomsDeclaration->fullName); 
                $message->subject("Customs Declaraction");
            }
        );
    }
}
