<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomsDeclaration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs_declaration', function (Blueprint $table) {
            $table->uuid('cd_id')->unique();
            $table->string('cd_first_name', 75);
            $table->string('cd_last_name', 75)->nullable();
            $table->string('cd_email', 50)->nullable();
            $table->date('cd_date_of_birth');
            $table->string('cd_occupation', 100);
            $table->string('cd_nationality', 2);
            $table->string('cd_passport_number', 15);
            $table->string('cd_address_in', 200);
            $table->string('cd_voyage_number', 5);
            $table->date('cd_arrival_date');
            $table->integer('cd_member')->default(0);
            $table->integer('cd_number_baggage')->default(0);
            $table->integer('cd_unaccompanied_baggage')->default(0);
            $table->boolean('cd_bring_animals')->default(false);
            $table->boolean('cd_bring_narcotics')->default(false);
            $table->boolean('cd_bring_currency')->default(false);
            $table->boolean('cd_bring_cigaretes')->default(false);
            $table->boolean('cd_bring_merchandise')->default(false);
            $table->boolean('cd_bring_goods')->default(false);
            $table->text('cd_remarks')->nullable();
            $table->string('cd_employee_remarks', 75)->nullable();
            $table->integer('cd_update_by')->nullable();
            $table->date('cd_update_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs_declaration');
    }
}
