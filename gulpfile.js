var gulp = require('gulp'),
    concat = require('gulp-concat'),
    cleancss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename-plus'),
    jshint = require('gulp-jshint'),
    livererload = require('gulp-livereload'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util');
var destinationStyleCss = 'public/css';
var destinationJavascript = 'public/js';

gulp.task('copyCleanStyleCss', function() {
    gulp.src([
            'resources/assets/plugins/pace/pace-theme-flash.css',
            'resources/assets/plugins/font-awesome/css/font-awesome.css',
            'resources/assets/plugins/select2/css/select2.min.css',
            'resources/assets/plugins/switchery/css/switchery.min.css',
            'resources/assets/plugins/bootstrap-datepicker/css/datepicker3.css',
            'resources/assets/css/dashboard.widgets.css',
            'resources/assets/css/pages-icons.css',
            'resources/assets/plugins/sweetalert2/dist/sweetalert2.min.css'
        ])
        .pipe(sourcemaps.init())
        .pipe(cleancss({ debug: true }, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(concat('bundle.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(destinationStyleCss))
});


gulp.task('copyLibraryJavascript', function() {
    gulp.src([
            'resources/assets/plugins/jquery/jquery-1.11.1.min.js',
            'resources/assets/plugins/tether/js/tether.min.js',
            'resources/assets/plugins/bootstrap/js/bootstrap.min.js',
            'resources/assets/plugins/jquery-ui/jquery-ui.min.js'
        ])
        .pipe(gulp.dest(destinationJavascript));
});


gulp.task('copyUglyJavascriptVendors', function() {
    gulp.src([
            'resources/assets/plugins/feather-icons/feather.min.js',
            'resources/assets/plugins/pace/pace.min.js',
            'resources/assets/plugins/modernizr.custom.js',
            'resources/assets/plugins/jquery/jquery-easy.js',
            'resources/assets/plugins/jquery-unveil/jquery.unveil.min.js',
            'resources/assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
            'resources/assets/plugins/jquery-actual/jquery.actual.min.js',
            'resources/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
            'resources/assets/plugins/select2/js/select2.full.min.js',
            'resources/assets/plugins/classie/classie.js',
            'resources/assets/plugins/switchery/js/switchery.min.js',
            'resources/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
            'resources/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js',
            'resources/assets/plugins/sweetalert2/dist/sweetalert2.min.js',
            'resources/assets/plugins/currency/simple.money.format.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendors.min.js'))
        .pipe(sourcemaps.write())
        .pipe(uglify())
        .pipe(gulp.dest(destinationJavascript));
});

gulp.task('copyUglyCustomJavascript', function() {
    gulp.src([
            'resources/assets/js/pages.min.js',
            'resources/assets/js/form_wizard.js',
            'resources/assets/js/scripts.js',
            'resources/assets/js/demo.js',
            'resources/assets/js/customs.min.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('custom.min.js'))
        .pipe(sourcemaps.write())
        .pipe(uglify())
        .pipe(gulp.dest(destinationJavascript));
});

gulp.task('default', ['copyCleanStyleCss', 'copyLibraryJavascript', 'copyUglyJavascriptVendors', 'copyUglyCustomJavascript'])
    //gulp.task('default', ['copyLibraryJavascript', 'copyUglyJavascriptVendors', 'copyUglyCustomJavascript'])