(function($) {
    'use strict';
    $(document).ready(function() {
        $('#rootwizard').bootstrapWizard({
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                if ($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show().removeClass('disabled hidden');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
                var li = navigation.find('li a.active').parent();
                var btnNext = $('#rootwizard').find('.pager .next').find('button');
                var btnPrev = $('#rootwizard').find('.pager .previous').find('button');
            },
            onNext: function(tab, navigation, index) {},
            onPrevious: function(tab, navigation, index) {},
            onInit: function() { $('#rootwizard ul').removeClass('nav-pills'); }
        });
        $('.remove-item').click(function() { $(this).parents('tr').fadeOut(function() { $(this).remove(); }); });
    });
})(window.jQuery);