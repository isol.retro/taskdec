<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/customs/welcome');
});

Route::get('/customs/welcome', function(){
    return view('component.default.welcome');
});

Route::get('/customs/booked/{id}','CustomsDeclarationController@getBookedRegister');
Route::get('/customs/released/{id}', 'CustomsDeclarationController@getReleased');
Route::get('/customs-declaration', 'CustomsDeclarationController@getRegister');
Route::post('/customs-declaration', 'CustomsDeclarationController@setRegister');
Route::post('/customs-draft', 'CustomsDeclarationController@setDraftRegister');
Route::post('/customs-loaded', 'CustomsDeclarationController@getReferenceNumberBooking');

Route::get('/notification-mail', function(){
    return view('component.mail.confirm');
});

Route::get('/mail', 'MailBlastController@sentMailBlast');


Route::get('/login', function(){
    return view('component.login.auth');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
